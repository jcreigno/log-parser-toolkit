/*jslint node : true, nomen: true, plusplus: true*/
"use strict";

var es = require('event-stream'),
    JSONStream = require('JSONStream'),
    split = require('split');
var re = /^INFO[ ]{2}\[[0-9]+\] ([0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2},[0-9]+) Terminal_Th - TIEMPOS OPERACION \(Mseg\): ([0-9]+) ms/;


module.exports = function () {
    return {
        name: "temps-reponse",
        reg: re,
        extract: function (res) {
            return {
                date: new Date(res[1].replace(' ', 'T').replace(',', '.')),
                value: +res[2]
            };
        }
        reduce: function(prev, current) {
            
        }
    };
};