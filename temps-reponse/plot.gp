reset
set terminal png
set title "répartition du temps de réponse"
set xlabel "temps de réponse en millisecondes"
set xrange [0:5000]

set ylabel "nb d'appels"
set grid
set style data boxes
set style fill solid 0.6
p file using 1:2 title "slot 1"
