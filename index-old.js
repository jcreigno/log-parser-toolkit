/*jslint node : true, nomen: true, plusplus: true, vars: true*/
"use strict";

var split = require('split'), es = require('event-stream'), path = require('path');

var SAMPLE_SIZE = 600000;
var start;
var current = 0;
var spread = {};

if (!process.argv[2]) {
    console.log("Provide module(s) name(s) has parameter");
    process.exit(-1);
}

var modules = process.argv.slice(2).map(function (el) {
    return require(path.join(__dirname, el))();
});

var modulesNames = modules.map(function names(m) {
    return m.name;
});

function pad(n) {
    return n < 10 ? '0' + n : n;
}
function writeDate(i) {
    var date = new Date(start + (i - 1) * SAMPLE_SIZE);
    var strDate = date.getFullYear() + '-' + pad(date.getMonth() + 1) + '-' + pad(date.getDate());
    strDate += ' ' + pad(date.getHours()) + ':' + pad(date.getMinutes()) + ':' + pad(date.getSeconds());
    return strDate;
}

function values(item) {
    return modulesNames.map(function (name) {
        return item[name] || 0;
    }).join(' ');
}


var t = es.through(function write(chunk) {
    var stream = this;
    modules.forEach(function matches(re) {
        var res = re.reg.exec(chunk.toString()), index;
        if (res) {
            var extracted = re.extract(res);
            var date = extracted.date || extracted;
            if (!start) {
                start = date.getTime();
            }
            index = Math.floor((date.getTime() - start) / SAMPLE_SIZE);
            if (index !== current) {
                stream.queue(writeDate(index) + ' ' + (index + 1) + ' ' + values(spread) + '\n');
                spread = {};
                current = index;
            }
            spread[re.name] = (spread[re.name] || 0) + 1;
        }
    });
}, function end() {
    var stream = this;
    stream.queue(writeDate(current) + ' ' + (current + 1) + ' ' + values(spread) + '\n');
    this.emit('end');
});

process.stdin.pipe(split()).pipe(t).pipe(process.stdout);
