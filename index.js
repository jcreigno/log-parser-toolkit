"use strict";
/*jslint node : true, nomen: true, plusplus: true, vars: true*/

var split = require('split'),
    AggregatorStream = require('./AggregatorStream'),
    path = require('path');

if (!process.argv[2]) {
    console.log("Provide module(s) name(s) has parameter");
    process.exit(-1);
}

var modules = process.argv.slice(2).map(function (el) {
    return require(path.join(__dirname, el))();
});

process.stdin.pipe(split()).pipe(new AggregatorStream({
    modules: modules
})).pipe(process.stdout);
