"use strict";
var Transform = require('stream').Transform;

function pad(n) {
    return n < 10 ? '0' + n : n;
}

class AggregatorStream extends Transform {
    constructor(options){
        super(options);
        this.modules = options.modules;
        this.modulesNames = this.modules.map((m) => {
            return m.name;
        });
        this.window = options.window || 1000;
        this.start = null;
        this.current = 0;
        this.spread = {};
    }
    values() {
        return this.modulesNames.map((name) => {
            return this.spread[name] || 0;
        }).join(' ');
    }
    writeDate(i){
        var date = new Date(this.start + (i - 1) * this.window);
        var strDate = date.getFullYear() + '-' + pad(date.getMonth() + 1) + '-' + pad(date.getDate());
        strDate += ' ' + pad(date.getHours()) + ':' + pad(date.getMinutes()) + ':' + pad(date.getSeconds());
        return strDate;
    }
    _transform(chunk, encoding, next){
        var stream = this;
        //console.log(chunk.toString());
        this.modules.forEach((re) => {
            var res = re.reg.exec(chunk.toString()), index;
            if (res) {
                var extracted = re.extract(res);
                var date = extracted.date || extracted;
                if (!date || !date.getTime()) {
                    console.log('fail to parse date', res);
                    return next();
                }
                if (!this.start) {
                    this.start = date.getTime();
                }
                index = Math.floor((date.getTime() - this.start) / this.window);
                //console.log('index', index, date.getTime(), this.start, this.window);
                if (index !== this.current) {
                    //console.log('pushing ', index, this.current);
                    this.push(this.writeDate(index) + ' ' + (index + 1) + ' ' + this.values() + '\n');
                    this.spread = {};
                    this.current = index;
                }
                this.spread[re.name] = (this.spread[re.name] || 0) + 1;
            }
            next();
        });
    }
    _flush(done) {
        done(null, this.writeDate(this.current) + ' ' + (this.current + 1) + ' ' + this.values() + '\n');
    } 
}

module.exports=AggregatorStream;