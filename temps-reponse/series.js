/*jslint node : true, nomen: true, plusplus: true*/
"use strict";

var es = require('event-stream'), split = require('split');
var re = /^INFO[ ]{2}\[[0-9]+\] [0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2},[0-9]+ Terminal_Th - TIEMPOS OPERACION \(Mseg\): ([0-9]+) ms/;

var spread = [];
var SAMPLE_SIZE = 100;

var t = es.through(function write(chunk) {
    var res = re.exec(chunk.toString()), elapsed, index, status;
    if (res) {
        elapsed = +res[1];
        index = Math.floor(elapsed / SAMPLE_SIZE);
        spread[index] = (spread[index] || 0) + 1;
    }
}, function end() {
    var stream = this;
    spread.forEach(function (val, i) {
        stream.queue((i * SAMPLE_SIZE) + ' ' + (val || 0) + '\n');
    });
    this.emit('end');
});

process.stdin.pipe(split()).pipe(t).pipe(process.stdout);