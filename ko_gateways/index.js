/*jslint node : true, nomen: true, plusplus: true, vars: true*/
"use strict";

var re = /^INFO  \[[0-9]+\] ([0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2},[0-9]+) .*? - Error en la venta: 300[0-9]/;

module.exports = function () {
    return {
        name: "gateway",
        reg: re,
        extract: function (res) {
            return new Date(res[1].replace(' ', 'T').replace(',', '.'));
        }
    };
};