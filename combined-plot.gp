reset
set terminal png
set title "répartition de la charge sur SIGMA"
set xlabel "temps"
set xdata time
set timefmt '%H:%M:%S'
set format x "%Hh"
set xrange ["00:00:00":]

set ylabel "nb d'appels"
set yrange [0:1750]
set grid
set style data boxes
set style fill solid 0.6
p file using 2:4 title "nb d'appels" lc rgb"#00AABE", '' using 2:5 title "nb de timeouts"  lc rgb"#E2001A" , '' using 2:6 title "nb ko gateway"  lc rgb"#F29000" 
