/*jslint node : true, nomen: true, plusplus: true, vars: true*/
"use strict";

var re = /^INFO[ ]{2}\[[0-9]+\] ([0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2},[0-9]+) Terminal_Th .*0000220500002503.*/;
//var re = /INFO[ ]{2}\[Terminal_th: [0-9]+\] ([0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2},[0-9]+) Terminal_Th - -+ VALIDACION TRAMA TPV -+/;

module.exports = function () {
    return { name : "comcod "+ 2205 + " tpv " + 2503, 
            reg : re,
            extract : function (res) {
                return new Date(res[1].replace(' ', 'T').replace(',', '.'));
            }
           };
};
