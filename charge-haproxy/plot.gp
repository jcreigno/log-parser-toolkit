reset
set terminal png size 1500,600
set title "charge haproxy"
set xlabel "temps"
set xdata time
set timefmt '%H:%M:%S'
set format x "%Hh"
set xrange ["00:00":]

set ylabel "nb d'appels"
set yrange [0:]
set grid
set style data boxes
set style fill solid 0.6
#p file using 2:4 title "nominal req/s" lc rgb"#00AABE", '' using 2:8 title "hier req/s"  lc rgb"#E2001A"
p file using 2:8 title "hier req/s"  lc rgb"#E2001A", '' using 2:4 title "nominal req/s" lc rgb"#00AABE"