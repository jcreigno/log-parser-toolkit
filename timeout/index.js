/*jslint node : true, nomen: true, plusplus: true, vars: true*/
"use strict";

var re = /^ERROR \[[0-9]+\] ([0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2},[0-9]+) Terminal_Th - Leer_Socket_PET\(\) - EXCEPCION POR SUPERACION TIMEOUT: Read timed out/;


module.exports = function () {
    return { 
        name : "timeout",
        reg : re,
        extract : function (res) {
            return new Date(res[1].replace(' ', 'T').replace(',', '.'));
        }
    };
};