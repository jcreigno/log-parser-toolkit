reset
set terminal png
set title "répartition de la charge sur SIGMA"
set xlabel "temps"
set xdata time
set timefmt '%H:%M:%S'
set format x "%Hh"
set xrange ["00:00":]

set ylabel "nb d'appels"
set yrange [0:150]
set grid
set style data boxes
set style fill solid 0.6
p file using 2:4 title "nombre de timeouts" lc rgb"#E2001A"
